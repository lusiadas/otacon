# otacon

> *A theme for [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish).*

[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v2.7.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-blue.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>

![screenshot](screenshot.png)

## Description

A simplified version of the [fox](https://github.com/Posnet/fish-theme) theme, optimized to display information adjusted to the terminal length.
### Statusbar

The status bar at the bottom of the image is rendered using `tmux` and can be found [here](https://gitlab.com/hthoreau/otacon_statusbar).

## Install

Either with omf
```fish
omf install otacon
```
or [fisherman](https://github.com/fisherman/fisherman)
```fish
fisher gitlab.com/lusiadas/otacon
```

---

Ⓐ Made in Anarchy. No wage slaves were economically coerced into the making of this work.
